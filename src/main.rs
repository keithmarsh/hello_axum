use axum::{
    handler::Handler,
    http::{HeaderMap, Method, Uri, Version},
    Json, Router,
};
use serde::Serialize;
use tracing::Level;
use std::{collections::HashMap, net::SocketAddr};
use tower_http::trace::{TraceLayer, DefaultMakeSpan, DefaultOnRequest};

#[tokio::main()]
async fn main() {
    tracing_subscriber::fmt::init();
    let app = Router::new()
        .fallback(echo_service.into_service())
        .layer(TraceLayer::new_for_http()
            .make_span_with(DefaultMakeSpan::new().include_headers(true))
            .on_request(DefaultOnRequest::new().level(Level::INFO))
);
    let addr = SocketAddr::from(([0, 0, 0, 0], 8080));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[derive(Serialize)]
struct RequestData {
    method: String,
    version: String,
    uri: String,
    headers: HashMap<String, String>,
    body: Option<String>,
}

async fn echo_service(
    method: Method,
    uri: Uri,
    version: Version,
    headers: HeaderMap,
    body: String,
) -> Json<RequestData> {
    let mut request_data = RequestData {
        method: method.to_string(),
        version: format!("{:?}", version),
        uri: uri.to_string(),
        headers: HashMap::new(),
        body: None,
    };
    for (name, value) in headers {
        if let Some(key) = name {
            request_data
                .headers
                .insert(key.to_string(), value.to_str().unwrap().to_string());
        }
    }
    if !body.is_empty() {
        request_data.body = Some(body);
    }
    Json(request_data)
}
